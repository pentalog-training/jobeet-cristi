<?php
namespace AppBundle\Utils;

class Jobeet
{
    /**
     * @param $text
     * @return string
     */
    public static function slugify($text)
    {
        if (empty($text)) {
            return 'n-a';
        }

        // replace non letter or digits by -
        $text = preg_replace('/\P{L}+/', '-', $text);

        // trim
        $text = trim($text, '-');

        // transliterate
        if (function_exists('iconv')) {
            $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);
        }

        // lowercase
        $text = strtolower($text);

        return $text;
    }
}
