<?php
namespace AppBundle\Controller;

use AppBundle\Entity\Subscriber;
use AppBundle\Form\Type\SubscriberType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class SubscriberController
 * @package AppBundle\Controller
 *
 * @Route("/subscriber")
 */
class SubscriberController extends Controller
{
    /**
     * @param Request $request
     *
     * @Route("/subscribe", name="subscribe")
     *
     * @return Response
     */
    public function subscribeAction(Request $request)
    {
        // create the form with appropriate fields
        $entity = new Subscriber();
        $form = $this->createForm(new SubscriberType(), $entity);

        // handle request
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            
            $entity->setActive(true);
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('sub_success'));
        }

        // render the form template
        return $this->render('Subscriber/subscribe.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/success", name="sub_success")
     *
     * @return Response
     */
    public function subscribeSuccessAction()
    {
        return $this->render('Subscriber/subscribe_success.html.twig');
    }
}
