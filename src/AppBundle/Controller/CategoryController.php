<?php
namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class CategoryController
 * @package AppBundle\Controller
 */
class CategoryController extends Controller
{
    /**
     * @Route("/{_locale}/category/{slug}", name="category_show",
     *     requirements={"_locale": "en|fr"}
     * )
     * @param Request $request
     * @param $slug
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function showAction(Request $request, $slug)
    {
        $category = $this->get('app.repository.category')->findOneBySlug($slug);

        if (!$category) {
            throw $this->createNotFoundException('Unable to find Category entity.');
        }

        $jobRepository = $this->get('app.repository.job');
        $lastUpdated = $this->get('app.helper.job')->getLastUpdatedAt();

        $category->setActiveJobs(
            $jobRepository->getActiveJobs($category->getId())
        );

        $query = $this->get('app.repository.job')->getJobsQuery($category->getId());

        $paginator = $this->get('knp_paginator');
        $jobs = $paginator->paginate(
            $query,
            $request->query->getInt('page', 1),
            10
        );

        $format = $request->getRequestFormat();

        return $this->render('category/show.' . $format . '.twig', array(
            'category' => $category,
            'jobs' => $jobs,
            'feedId' => sha1(
                $this
                    ->get('router')
                    ->generate(
                        'category_show',
                        array('slug' => $category->getSlug(), 'format' => 'atom'),
                        true
                    )
            ),
            'lastUpdated' => $lastUpdated->format(DATE_ATOM),
            'hasPaginator' => true,
        ));
    }
}
