<?php
namespace AppBundle\Controller;

use AppBundle\Entity\Category;
use AppBundle\Entity\Job;
use JMS\Serializer\SerializationContext;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\Serializer;

/**
 * Class RestApiController
 *
 * @package AppBundle\Controller
 *
 * @Route("/restapi")
 */
class RestApiController extends Controller
{
    /**
     * TASKS:
     * + Get jobs
     * + Get job
     * + Get categories
     * + Get category
     * + Add job
     * + Add category
     * + Delete job
     * + Delete category
     * + Update job
     * + Update category
     */

    /**
     * Get all jobs grouped by category
     *
     * @Route("/jobs")
     * @Method("GET")
     * @return Response Returns data as JSON
     */
    public function getJobs()
    {
        $categoryRepository = $this->container->get('app.repository.category');
        $jobs = $categoryRepository->getWithJobs();

        $serializer = $this->container->get('jms_serializer');
        $json = $serializer->serialize(
            $jobs,
            'json',
            SerializationContext::create()->setGroups(['category-details'])
        );

        return new Response($json, 200, ['Content-Type' => 'text/json']);
    }

    /**
     * Get one job by ID
     *
     * @Route("/jobs/{job}")
     * @Method("GET")
     *
     * @param Job|int $job Job ID
     * @ParamConverter("job", class="AppBundle:Job")
     *
     * @return Response
     */
    public function getJob(Job $job)
    {
        $serializer = $this->container->get('jms_serializer');
        $json = $serializer->serialize(
            $job,
            'json',
            SerializationContext::create()->setGroups(['job-details'])
        );

        return new Response($json, 200, ['Content-Type' => 'text/json']);
    }

    /**
     * Get all categories
     *
     * @Route("/categories/")
     * @Method("GET")
     */
    public function getCategories()
    {
        $categoryRepository = $this->container->get('app.repository.category');
        $categories = $categoryRepository->getCategories();

        $serializer = $this->container->get('jms_serializer');
        $json = $serializer->serialize(
            $categories,
            'json',
            SerializationContext::create()->setGroups(['category-list'])
        );

        return new Response($json, 200, ['Content-Type' => 'text/json']);
    }

    /**
     * Get one category by ID
     *
     * @Route("/categories/{category}")
     * @Method("GET")
     *
     * @param Category|int $category Category ID
     * @ParamConverter("category", class="AppBundle:Category")
     *
     * @return Response
     */
    public function getCategory(Category $category)
    {
        $serializer = $this->container->get('jms_serializer');
        $json = $serializer->serialize(
            $category,
            'json',
            SerializationContext::create()->setGroups(['category-list'])
        );

        return new Response($json, 200, ['Content-Type' => 'text/json']);
    }

    /**
     * Add a new job
     *
     * @Route("/job/category/{category}")
     * @method("POST")
     *
     * @param Category $category
     * @param Request $request
     * @ParamConverter("category", class="AppBundle:Category")
     *
     * @return Response Returns the ID of the added job
     */
    public function addJob(Category $category, Request $request)
    {
        $serializer = $this->container->get('jms_serializer');

        /** @var Job $job */
        $job = $serializer->deserialize($request->getContent(), 'AppBundle\Entity\Job', 'json');

        $job->setCategory($category);

        $em = $this->getDoctrine()->getManager();
        $em->persist($job);
        $em->flush();

        return new JsonResponse(
            ['success' => true, 'id' => $job->getId()],
            201
        );
    }

    /**
     * Add a new category
     *
     * @Route("/category")
     * @Method("POST")
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function addCategory(Request $request)
    {
        $serializer = $this->container->get('jms_serializer');

        /** @var Category $category */
        $category = $serializer->deserialize($request->getContent(), 'AppBundle\Entity\Category', 'json');

        $em = $this->getDoctrine()->getManager();
        $em->persist($category);
        $em->flush();

        return new JsonResponse(
            ['success' => true, 'id' => $category->getId()],
            201
        );
    }

    /**
     * Delete job by ID
     *
     * @Route("/job/{job}")
     * @Method("DELETE")
     *
     * @param Job|int $job Job ID of the job that will be deleted
     * @ParamConverter("job", class="AppBundle:Job")
     *
     * @return JsonResponse
     */
    public function deleteJob(Job $job)
    {
        $em = $this->getDoctrine()->getManager();
        $em->remove($job);
        $em->flush();

        return new JsonResponse(
            ['success' => true, 'id' => $job->getId()],
            200
        );
    }

    /**
     * Delete category by ID
     *
     * @Route("/category/{category}")
     * @Method("DELETE")
     *
     * @param Category|int $category Category ID of the category that will be deleted
     * @ParamConverter("category", class="AppBundle:Category")
     *
     * @return JsonResponse
     */
    public function deleteCategory(Category $category)
    {
        $em = $this->getDoctrine()->getManager();
        $em->remove($category);
        $em->flush();

        return new JsonResponse(
            ['success' => true, 'id' => $category->getId()],
            200
        );
    }

    /**
     * Update job
     *
     * @Route("/job/category/{category}")
     * @method("PUT")
     *
     * @param Category|int $category ID of the category to which the job that is going to be updated belongs
     * @ParamConverter("category", class="AppBundle:Category")
     * @param Request $request
     *
     * @return Response Return the ID of the updated job
     */
    public function updateJob(Category $category, Request $request)
    {
        /** @var Serializer $serializer */
        $serializer = $this->container->get('jms_serializer');

        /** @var Job $job */
        $job = $serializer->deserialize($request->getContent(), 'AppBundle\Entity\Job', 'json');

        /** * @var Category $category */
        $job->setCategory($category);

        $em = $this->getDoctrine()->getManager();
        $em->merge($job);
        $em->flush();

        return new JsonResponse(
            ['success' => true, 'id' => $job->getId()],
            200
        );
    }

    /**
     * Update category
     *
     * @Route("/category")
     * @method("PUT")
     *
     * @param Request $request
     *
     * @return Response Return the ID of the updated category
     */
    public function updateCategory(Request $request)
    {
        /** @var Serializer $serializer */
        $serializer = $this->container->get('jms_serializer');

        /** @var Category $category */
        $category = $serializer->deserialize($request->getContent(), 'AppBundle\Entity\Category', 'json');

        $em = $this->getDoctrine()->getManager();
        $em->merge($category);
        $em->flush();

        return new JsonResponse(
            ['success' => true, 'id' => $category->getId()],
            200
        );
    }
}
