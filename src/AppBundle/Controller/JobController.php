<?php
namespace AppBundle\Controller;

use AppBundle\Entity\Category;
use AppBundle\Entity\Job;
use AppBundle\Form\Type\JobType;
use Doctrine\ORM\EntityManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class JobController
 * @package AppBundle\Controller
 */
class JobController extends Controller
{
    /**
     * @Route("/{_locale}", name="job_index", requirements={"_locale": "en|fr"},
     *     defaults={"_locale" = "en"}
     * )
     * @param Request $request
     * @return Response
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        /** @var Category[] $categories */
        $categories = $em->getRepository('AppBundle:Category')->getWithJobs();

        foreach ($categories as $category) {
            $category->setActiveJobs($em->getRepository('AppBundle:Job')->getActiveJobs(
                $category->getId(),
                $this->getParameter('max_jobs_on_homepage')
            ));

            $activeJobs = $em->getRepository('AppBundle:Job')->countActiveJobs($category->getId());
            $category->setMoreJobs(
                $activeJobs - $this->getParameter('max_jobs_on_homepage')
            );
        }

        $format = $request->getRequestFormat();

        $lastUpdated = $this->get('app.helper.job')->getLastUpdatedAt();
        return $this->render('job/index.' . $format . '.twig', array(
            'categories' => $categories,
            'lastUpdated' => $lastUpdated->format(DATE_ATOM),
            'feedId' => sha1($this->get('router')->generate('job_index', array('_format' => 'atom'), true)),
            'hasPaginator' => false,
        ));
    }

    /**
     * Finds and displays a Job entity.
     *
     * @Route(
     *     "/{_locale}/job/{company}/{location}/{id}/{position}/{modal}",
     *     name="job_show",
     *     requirements={"id": "\d+", "_locale": "en|fr"},
     *     defaults={"modal" = ""}
     * )
     * @Method("GET")
     * @param integer $id
     * @param boolean $modal Whether the job description will be displayed in a modal window
     * @param Request $request
     * @return Response
     */
    public function showAction($id, $modal, Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        /** @var Job $entity */
        $entity = $em->getRepository('AppBundle:Job')->getActiveJob($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find job entry');
        }

        $session = $request->getSession();

        // fetch jobs already stored in the job history

        $jobs = $session->get('job_history', array());

        // store the job as an array so we can put it in the session and avoid entity serialize errors
        $job = array(
            'id' => $entity->getId(),
            'position' => $entity->getPosition(),
            'company' => $entity->getCompany(),
            'companyslug' => $entity->getCompanySlug(),
            'locationslug' => $entity->getLocationSlug(),
            'positionslug' => $entity->getPositionSlug()
        );

        if (!in_array($job, $jobs)) {
            // add the current job at the beginning of the array
            array_unshift($jobs, $job);

            // store the new job history back into the session
            $session->set('job_history', array_slice($jobs, 0, 3));
        }

        $deleteForm = $this->createTokenForm($id);

        if ($modal === 'modal') {
            return $this->render('job/show_modal.html.twig', array(
                'entity' => $entity,
                'delete_form' => $deleteForm->createView(),
            ));
        } else {
            return $this->render('job/show.html.twig', array(
                'entity' => $entity,
                'delete_form' => $deleteForm->createView(),
            ));
        }
    }

    /**
     * Creates a form to delete a Job entity by id.
     *
     * @param $token
     * @return \Symfony\Component\Form\Form The form
     * @internal param mixed $id The entity id
     *
     */
    private function createTokenForm($token)
    {
        return $this->createFormBuilder(array('token' => $token))
            ->add('token', HiddenType::class)
            ->getForm();
    }

    /**
     * @Route("/{_locale}/job/create", name="job_create", requirements={"_locale": "en|fr"})
     *
     * @param Request $request User request
     *
     * @return Response
     */
    public function createAction(Request $request)
    {
        $entity = new Job();
        $entity->setType('full-time');
        $form = $this->createForm(new JobType(), $entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity->publish();

            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('job_preview', array(
                'company' => $entity->getCompanySlug(),
                'location' => $entity->getLocationSlug(),
                'token' => $entity->getToken(),
                'position' => $entity->getPositionSlug(),
            )));
        }

        return $this->render('job/new.html.twig', array(
            'entity' => $entity,
            'form' => $form->createView(),
        ));
    }

    /**
     * Edits a Job entity
     *
     * @Route("/{_locale}/{token}/edit", name="job_edit", requirements={"_locale": "en|fr"})
     * @param $token
     * @return Response
     */
    public function editAction($token)
    {
        $em = $this->getDoctrine()->getManager();

        /** @var Job $entity */
        $entity = $em->getRepository('AppBundle:Job')->findOneByToken(array('token' => $token));

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Job entity.');
        }

        if (!$entity->isActive()) {
            throw $this->createNotFoundException('Job is activated and cannot be edited.');
        }

        $editForm = $this->createForm(new JobType(), $entity);
        $deleteForm = $this->createTokenForm($token);

        return $this->render('job/edit.html.twig', array(
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * @Route("/{_locale}/{token}/extend", name="job_extend", requirements={"_locale": "en|fr"})
     * @Method("POST")
     * @param Request $request
     * @param $token
     * @return RedirectResponse
     */
    public function extendAction(Request $request, $token)
    {
        $form = $this->createTokenForm($token);

        $form->handleRequest($request);

        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('AppBundle:Job')->findOneByToken($token);

        if ($form->isValid()) {
            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Job entity.');
            }

            if (!$entity->extend()) {
                throw $this->createNotFoundException('Unable to extend the Job');
            }

            $em->persist($entity);
            $em->flush();

            $this->get('session')
                ->getFlashBag()
                ->add(
                    'notice',
                    sprintf(
                        'Your job validity has been extended until %s',
                        $entity->getExpiresAt()->format('m/d/Y')
                    )
                );
        }

        return $this->redirect($this->generateUrl('job_preview', array(
            'company' => $entity->getCompanySlug(),
            'location' => $entity->getLocationSlug(),
            'token' => $entity->getToken(),
            'position' => $entity->getPositionSlug(),
        )));
    }

    /**
     * @Route("/{_locale}/{token}/update", name="job_update", requirements={"_locale": "en|fr"})
     *
     * @param Request $request
     * @param $token
     *
     * @return RedirectResponse|Response
     */
    public function updateAction(Request $request, $token)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:Job')->findOneByToken(array('token' => $token));

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Job entity');
        }

        $editForm = $this->createForm(new JobType(), $entity);
        $deleteForm = $this->createTokenForm($token);

        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('job_preview', array(
                'company' => $entity->getCompanySlug(),
                'location' => $entity->getLocationSlug(),
                'token' => $entity->getToken(),
                'position' => $entity->getPositionSlug(),
            )));
        }

        return $this->render('job/edit.html.twig', array(
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * @Route("/{_locale}/{token}/delete", name = "job_delete", requirements={"_locale": "en|fr"})
     *
     * @param Request $request
     * @param $token
     *
     * @return RedirectResponse
     */
    public function deleteAction(Request $request, $token)
    {
        $form = $this->createTokenForm($token);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('AppBundle:Job')->findOneByToken(array('token' => $token));

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Job entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('job_index'));
    }

    /**
     * @Route("/{_locale}/{token}/publish", name="job_publish", requirements={"_locale": "en|fr"})
     * @Method("POST")
     *
     * @param Request $request
     * @param $token
     * @return RedirectResponse
     */
    public function publishAction(Request $request, $token)
    {
        $form = $this->createTokenForm($token);
        $form->handleRequest($request);

        /** @var EntityManager $em */
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('AppBundle:Job')->findOneByToken(array('token' => $token));

        if ($form->isValid()) {
            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Job entity.');
            }

            $entity->publish();
            $em->persist($entity);
            $em->flush();

            $this->get('session')->getFlashBag()->add('notice', 'Your job is now online for 30 days.');
        }

        return $this->redirect($this->generateUrl('job_preview', array(
            'company' => $entity->getCompanySlug(),
            'location' => $entity->getLocationSlug(),
            'token' => $entity->getToken(),
            'position' => $entity->getPositionSlug()
        )));
    }

    /**
     * @Route("/{_locale}/preview/{company}/{location}/{token}/{position}", name="job_preview",
     *     requirements={"token": "\w+", "_locale": "en|fr"}
     * )
     * @param $token
     * @return Response
     */
    public function previewAction($token)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:Job')->findOneByToken(array('token' => $token));

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find the Job entity');
        }

        $deleteForm = $this->createTokenForm($entity->getId());
        $publishForm = $this->createTokenForm($entity->getToken());
        $extendForm = $this->createTokenForm($entity->getToken());

        return $this->render('job/show.html.twig', array(
            'entity' => $entity,
            'delete_form' => $deleteForm->createView(),
            'publish_form' => $publishForm->createView(),
            'extend_form' => $extendForm->createView(),
        ));
    }

    /**
     * Get all jobs of a specific category
     *
     * @Route("/get/jobs/{categoryId}")
     * @param int $categoryId ID of the category
     *
     * @return Response
     */
    public function getJobsAction($categoryId)
    {
        $jobRepository = $this->container->get('app.repository.job');
        $jobs = $jobRepository->getActiveJobs($categoryId, $this->getParameter('max_jobs_on_homepage'));

        return $this->render(
            'job/list.html.twig',
            ['jobs' => $jobs]
        );
    }
}
