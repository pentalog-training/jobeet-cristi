<?php
namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class SearchController
 * @package AppBundle\Controller
 */
class SearchController extends Controller
{
    /**
     * @Route("/{_locale}/search", name="job_search", requirements={"_locale": "en|fr"})
     * @param Request $request
     * @return RedirectResponse|Response
     */
    public function searchAction(Request $request)
    {
        // get the query string
        $q = $request->get('query');

        // redirect to the main page if no query found
        if (!$q) {
            if (!$request->isXmlHttpRequest()) {
                return $this->redirect($this->generateUrl('job_index'));
            } else {
                return new Response('No results.');
            }
        }

        $query = $this->get('app.repository.job')->searchForJob($q);

        $paginator = $this->get('knp_paginator');
        $jobs = $paginator->paginate(
            $query,
            $request->query->getInt('page', 1),
            10
        );

        if ($request->isXmlHttpRequest()) {
            return $this->render('job/list.html.twig', array(
                'jobs' => $jobs,
                'hasPaginator' => true,
            ));
        }

        return $this->render('job/search.html.twig', array(
            'jobs' => $jobs,
            'hasPaginator' => true,
        ));
    }
}
