<?php
namespace AppBundle\Controller;

use AppBundle\Entity\Affiliate;
use Doctrine\ORM\EntityManager;
use Exception;
use Sonata\AdminBundle\Datagrid\ProxyQueryInterface;
use Sonata\AdminBundle\Controller\CRUDController as Controller;
use Swift_Message;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

class AffiliateAdminController extends Controller
{
    public function batchActionActivate(ProxyQueryInterface $selectedModelQuery)
    {
        if ($this->admin->isGranted('EDIT') === false || $this->admin->isGranted('DELETE') === false) {
            throw new AccessDeniedException();
        }

        $modelManager = $this->admin->getModelManager();

        $selectedModels = $selectedModelQuery->execute();

        try {
            foreach ($selectedModels as $selectedModel) {
                $selectedModel->activate();
                $modelManager->update($selectedModel);

                $message = Swift_Message::newInstance()
                    ->setSubject('Jobeet affiliate token')
                    ->setFrom('cristigar@engineer.com')
                    ->setTo($selectedModel->getEmail())
                    ->setBody(
                        $this->renderView(
                            'Affiliate/email.txt.twig',
                            array('affiliate' => $selectedModel->getToken())
                        )
                    );

                $this->get('mailer')->send($message);
            }
        } catch (Exception $e) {
            $this->get('session')->setFlash('sonata_flash_error', $e->getMessage());

            return new RedirectResponse($this->admin->generateUrl(
                'list',
                $this->admin->getFilterParameters()
            ));
        }

        $this->get('session')->getFlashBag()->add(
            'sonata_flash_success',
            sprintf('The selected accounts have been activated')
        );

        return new RedirectResponse($this->admin->generateUrl('list', $this->admin->getFilterParameters()));
    }

    public function batchActionDeactivate(ProxyQueryInterface $selectedModelQuery)
    {
        if ($this->admin->isGranted('EDIT') === false
            || $this->admin->isGranted('DELETE') === false
        ) {
            throw new AccessDeniedException();
        }

        $modelManager = $this->admin->getModelManager();

        $selectedModels = $selectedModelQuery->execute();

        try {
            foreach ($selectedModels as $selectedModel) {
                $selectedModel->deactivate();
                $modelManager->update($selectedModel);
            }
        } catch (Exception $e) {
            $this->get('session')->getFlashBag()->add('sonata_flash_error', $e->getMessage());

            return new RedirectResponse($this->admin->generateUrl(
                'list',
                $this->admin->getFilterParameters()
            ));
        }

        $this->get('session')->getFlashBag()->add(
            'sonata_flash_success',
            sprintf('The selected accounts have been deactivated')
        );

        return new RedirectResponse($this->admin->generateUrl(
            'list',
            $this->admin->getFilterParameters()
        ));
    }

    public function activateAction($id)
    {
        if ($this->admin->isGranted('EDIT') === false) {
            throw new AccessDeniedException();
        }
        /** @var EntityManager $em */
        $em = $this->getDoctrine()->getManager();
        /** @var Affiliate $affiliate */
        $affiliate = $em->getRepository('AppBundle:Affiliate')->findOneById($id);

        try {
            $affiliate->setActive(true);
            $em->flush();

            $message = Swift_Message::newInstance()
                ->setSubject('Jobeet Affiliate Token')
                ->setFrom('cristigar@engineer.com')
                ->setTo($affiliate->getEmail())
                ->setBody(
                    $this->renderView('Affiliate/email.txt.twig', array('affiliate' => $affiliate->getToken()))
                );
            $this->get('mailer')->send($message);
        } catch (Exception $e) {
            $this->get('session')->getFlashBag()->add('sonata_flash_error', $e->getMessage());
        }

        return new RedirectResponse($this->admin->generateUrl(
            'list',
            $this->admin->getFilterParameters()
        ));
    }

    public function deactivateAction($id)
    {
        if ($this->admin->isGranted('EDIT') === false) {
            throw new AccessDeniedException();
        }


        $em = $this->getDoctrine()->getManager();
        $affiliate = $em->getRepository('AppBundle:Affiliate')->findOneById($id);

        try {
            $affiliate->setActive(false);
            $em->flush();
        } catch (Exception $e) {
            $this->get('session')->getFlashBag()->add('sonata_flash_error', $e->getMessage());

            return new RedirectResponse($this->admin->generateUrl('list', $this->admin->getFilterParameters()));
        }

        return new RedirectResponse($this->admin->generateUrl('list', $this->admin->getFilterParameters()));
    }
}
