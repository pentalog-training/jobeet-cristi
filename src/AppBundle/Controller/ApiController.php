<?php
namespace AppBundle\Controller;

use Doctrine\ORM\EntityManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class ApiController extends Controller
{

    /**
     * @Route(
     *     "/api/{token}/jobs.{_format}",
     *     name="api_list"
     * )
 *     requirements={
 *         "_format": "xml|json|yaml"
 *     }
     * @param Request $request
     * @param $token
     * @return Response
     */
    public function listAction(Request $request, $token)
    {
        /** @var EntityManager $em */
        $em = $this->getDoctrine()->getManager();

        $jobs = array();

        $rep = $em->getRepository('AppBundle:Affiliate');
        $affiliate = $rep->getForToken($token);

        if (!$affiliate) {
            throw $this->createNotFoundException('This affiliate account does not exist!');
        }

        $rep = $em->getRepository('AppBundle:Job');
        $activeJobs = $rep->getActiveJobs(null, null, null, $affiliate->getId());

        foreach ($activeJobs as $job) {
            $jobs[
                $this
                    ->get('router')
                    ->generate(
                        'job_show',
                        array(
                            'company' => $job->getCompanySlug(),
                            'location' => $job->getLocationSlug(),
                            'id' => $job->getId(),
                            'position' => $job->getPositionSlug()),
                        true
                    )] = $job->asArray($request->getHost());
        }

        $format = $request->getRequestFormat();
        $jsonData = json_encode($jobs);

        if ($format == 'json') {
            $headers = array('Content-Type' => 'application/json');
            $response = new Response($jsonData, 200, $headers);

            return $response;
        }

        return $this->render(
            ':Api:jobs.' . $format . '.twig',
            array('jobs' => $jobs)
        );
    }
}
