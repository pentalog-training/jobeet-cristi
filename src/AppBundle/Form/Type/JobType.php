<?php
namespace AppBundle\Form\Type;

use AppBundle\Entity\Job;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\FormBuilder;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class JobType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
//        $publicDescription = $this->container

        $builder
            ->add('type', ChoiceType::class, array(
                'choices' => Job::getTypes(),
                'expanded' => true,
            ))
            ->add('category')
            ->add('company')
            ->add('file', FileType::class, array(
                'label' => 'Company Logo',
                'required' => false
            ))
            ->add('url', null, array(
                'label' => 'URL'
            ))
            ->add('position')
            ->add('location')
            ->add('description')
            ->add('howToApply', null, array('label' => 'How to apply?'))
            ->add('public', null, array(
                'label' => 'Public*',
                'attr' => array(
                    'title' => 'can_be_pub_on_aff'
                ),
            ))
            ->add('email')
            ;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Job',
        ));
    }

    /**
     * @return string
     */
    public function getBlockPrefix()
    {
        return 'job';
    }
}
