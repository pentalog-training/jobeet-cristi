<?php

namespace AppBundle\Services;

use AppBundle\Repository\JobRepository;

class JobHelper
{
    private $jobRepository;

    /**
     * JobHelper constructor.
     */
    public function __construct(JobRepository $jobRepository)
    {
        $this->jobRepository = $jobRepository;
    }

    public function getLastUpdatedAt()
    {
        $latestJob = $this->jobRepository->getLatestPost();

        $lastUpdatedAt = new \DateTime();
        if ($latestJob && $latestJob->getUpdatedAt()) {
            $lastUpdatedAt = $latestJob->getUpdatedAt();
        }

        return $lastUpdatedAt;
    }
}
