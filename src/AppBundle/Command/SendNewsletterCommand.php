<?php
namespace AppBundle\Command;

use AppBundle\Entity\Subscriber;
use Swift_Message;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class SendNewsletterCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('app:newsletter:send')
            ->setDescription('Send newsletters to subscribers');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|null|void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $periodicities = Subscriber::getPeriodicityValues();
        $periodicitySeconds = Subscriber::getPeriodicitySeconds();

        $categories = $this->getContainer()->get('app.repository.category')->getWithJobs();

        $mailer = $this->getContainer()->get('mailer');
        $mailCounter = 0;

        foreach ($periodicities as $periodicity) {
            $subscribers = $this->getContainer()->get('app.repository.subscriber')->getSubscribers($periodicity);

            $fromDate = date('Y-m-d H:i:s', time() - $periodicitySeconds[$periodicity]);
            foreach ($categories as $category) {
                $jobs[$category->getId()] = $this
                    ->getContainer()
                    ->get('app.repository.job')
                    ->getJobs($fromDate, $category->getId(), 10);
            }

            foreach ($subscribers as $subscriber) {
                /** @var Subscriber $subscriber */
                $subscriber->getCategories();

                $message = Swift_Message::newInstance()
                    ->setSubject('Jobeet Newsletter')
                    ->setFrom('jnl_no_reply@jobeet.local')
                    ->setTo($subscriber->getEmail())
                    ->setBody(
                        $this->getContainer()->get('templating')->render('Subscriber/newsletter.html.twig', array(
                            'categories' => $subscriber->getCategories(),
                            'jobs' => $jobs,
                        )),
                        'text/html'
                    );
                $mailer->send($message);
                $mailCounter++;
            }
        }
        $output->writeln($mailCounter . ' sent messages.');
    }
}
