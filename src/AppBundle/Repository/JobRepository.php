<?php
namespace AppBundle\Repository;

use AppBundle\Entity\Job;
use Doctrine\ORM\EntityRepository;

/**
 * JobRepository
 *
 * @package AppBundle\Repository
 * @method Job findOneByToken(string $token)
 */
class JobRepository extends EntityRepository
{
    /**
     * @param null $categoryId
     * @param null $max
     * @param null $offset
     * @param null $affiliateId
     * @return Job[]
     */
    public function getActiveJobs($categoryId = null, $max = null, $offset = null, $affiliateId = null)
    {
        $qb = $this->createQueryBuilder('j')
            ->where('j.expiresAt > :date')
            ->setParameter('date', date('Y-m-d H:i:s', time()))
            ->andWhere('j.active = :active')
            ->setParameter('active', 1)
            ->orderBy('j.expiresAt', 'DESC');

        if ($max) {
            $qb->setMaxResults($max);
        }

        if ($offset) {
            $qb->setFirstResult($offset);
        }

        if ($categoryId) {
            $qb->andWhere('j.category = :categoryId')
                ->setParameter('categoryId', $categoryId);
        }

        if ($affiliateId) {
            $qb->leftJoin('j.category', 'c')
                ->leftJoin('c.affiliates', 'a')
                ->andWhere('a.id = :affiliateId')
                ->setParameter('affiliateId', $affiliateId);
        }

        return $qb->getQuery()->getResult();
    }

    /**
     * @param $id
     * @return Job|null
     */
    public function getActiveJob($id)
    {
        $query = $this->createQueryBuilder('j')
            ->where('j.id = :id')
            ->setParameter('id', $id)
            ->andWhere('j.expiresAt > :date')
            ->setParameter('date', date('Y-m-d H:i:s', time()))
            ->andWhere('j.active = :activated')
            ->setParameter('activated', 1)
            ->setMaxResults(1)
            ->getQuery();

        return $query->getOneOrNullResult();

    }

    /**
     * @param null $categoryId
     * @return mixed
     */
    public function countActiveJobs($categoryId = null)
    {
        $qb = $this->createQueryBuilder('j')
            ->select('count(j.id)')
            ->where('j.expiresAt > :date')
            ->setParameter('date', date('Y-m-d H:i:s', time()))
            ->andWhere('j.active = :activated')
            ->setParameter('activated', 1);

        if ($categoryId) {
            $qb->andWhere('j.category = :categoryId')
                ->setParameter('categoryId', $categoryId);
        }

        $query = $qb->getQuery();

        return $query->getSingleScalarResult();
    }

    /**
     * @param $days
     * @return mixed
     */
    public function cleanup($days)
    {
        $query = $this->createQueryBuilder('j')
            ->delete()
            ->where('j.active IS NULL')
            ->andWhere('j.createdAt < :createdAt')
            ->setParameter('createdAt', date('Y-m-d', time() - 86400 * $days))
            ->getQuery();

        return $query->execute();
    }

    /**
     * @param null $categoryId
     * @return Job|null
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function getLatestPost($categoryId = null)
    {
        $query = $this->createQueryBuilder('j')
            ->where('j.expiresAt > :date')
            ->setParameter('date', date('Y-m-d H:i:s', time()))
            ->andWhere('j.active = :activated')
            ->setParameter('activated', 1)
            ->orderBy('j.expiresAt', 'DESC')
            ->setMaxResults(1);

        if ($categoryId) {
            $query->andWhere('j.category = :categoryId')
                ->setParameter('categoryId', $categoryId);
        }

        return $query->getQuery()->getOneOrNullResult();
    }

    /**
     * @param $query
     * @return \Doctrine\ORM\Query
     */
    public function searchForJob($query)
    {
        $qb = $this->createQueryBuilder('j')
            ->where('j.position LIKE :query')
            ->orWhere('j.type LIKE :query')
            ->orWhere('j.company LIKE :query')
            ->orWhere('j.location LIKE :query')
            ->setParameter('query', '%' . $query . '%')
            ->andWhere('j.active = :active')
            ->setParameter('active', 1);

        return $qb->getQuery();
    }

    /**
     * @param $categoryId
     * @return \Doctrine\ORM\Query
     */
    public function getJobsQuery($categoryId)
    {
        $qb = $this->createQueryBuilder('j')
            ->where('j.expiresAt > :date')
            ->setParameter('date', date('Y-m-d H:i:s', time()))
            ->andWhere('j.active = :activated')
            ->setParameter('activated', 1)
            ->andWhere('j.category = :categoryId')
            ->setParameter('categoryId', $categoryId)
            ->orderBy('j.expiresAt', 'DESC');

        return $qb->getQuery();
    }

    /**
     * @param $fromDate
     * @param null $categoryId
     * @param null $max
     * @return array
     */
    public function getJobs($fromDate, $categoryId, $max = null)
    {
        $qb = $this->createQueryBuilder('j')
            ->where('j.createdAt >= :fromDate')
            ->setParameter('fromDate', $fromDate)
            ->andWhere('j.category = :categoryId')
            ->setParameter('categoryId', $categoryId);

        if ($max) {
            $qb->setMaxResults($max);
        }

        return $qb->getQuery()->getResult();
    }
}
