<?php
namespace AppBundle\Repository;

use AppBundle\Entity\Subscriber;
use Doctrine\ORM\EntityRepository;

/**
 * Class SubscriberRepository
 * @package AppBundle\Repository
 */
class SubscriberRepository extends EntityRepository
{
    /**
     * @param string $periodicity
     * @return Subscriber|array
     */
    public function getSubscribers($periodicity = 'daily')
    {
        $query = $this->createQueryBuilder('s')
            ->addSelect('c')
            ->leftJoin('s.categories', 'c')
            ->where('s.periodicity = :periodicity')
            ->setParameter('periodicity', $periodicity)
            ->getQuery();

        return $query->getResult();
    }
}
