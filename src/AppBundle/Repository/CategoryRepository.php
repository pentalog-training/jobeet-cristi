<?php
namespace AppBundle\Repository;

use AppBundle\Entity\Category;
use Doctrine\ORM\EntityRepository;

/**
 * Class CategoryRepository
 * @package AppBundle\Repository
 * @method Category findOneBySlug(string $slug)
 * @method Category findOneById(int $categoryId)
 */
class CategoryRepository extends EntityRepository
{
    /**
     * @return Category[]
     */
    public function getWithJobs()
    {
        $query = $this->createQueryBuilder('c')
            ->addSelect('j')
            ->leftJoin('c.jobs', 'j')
            ->where('j.expiresAt > :date')
            ->andWhere('j.active = :activated')
            ->setParameter('date', date('Y-m-d H:i:s', time()))
            ->setParameter('activated', 1);

        return $query->getQuery()->getResult();
    }

    /**
     * @param $fromDate
     * @return array|Category[]
     */
    public function getCategoriesForNewsletter($fromDate)
    {
        $query = $this->createQueryBuilder('c')
            ->addSelect('j')
            ->leftJoin('c.jobs', 'j')
            ->where('j.createdAt >= :fromDate')
            ->setParameter('fromDate', $fromDate)
            ->orderBy('c.id')
            ->addOrderBy('c.createdAt')
            ->getQuery();

        return $query->getResult();
    }

    /**
     * Gets a category based on its ID
     *
     * @param int $categoryId ID of the category to look for
     *
     * @return Category
     */
    public function getCategoryById($categoryId)
    {
        return $this->findOneById($categoryId);
    }

    /**
     * Get all categories
     */
    public function getCategories()
    {
        $categories = $this->findAll();

        return $categories;
    }
}
