<?php
namespace AppBundle\Entity;

use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class Subscriber
 * @package AppBundle\Entity
 *
 * @ORM\Entity(repositoryClass="AppBundle\Repository\SubscriberRepository")
 * @ORM\Table(name="subscriber")
 */
class Subscriber
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, unique=true)
     *
     * @Assert\NotBlank()
     * @Assert\Email()
     */
    private $email;

    /**
     * @var Category[]|ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="Category")
     *
     * @Assert\NotBlank()
     */
    private $categories;

    /**
     * @ORM\Column(type="string", length=255)
     *
     * @Assert\NotBlank()
     * @Assert\Choice(callback = "getPeriodicityValues")
     */
    private $periodicity;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $lastSentMailAt;

    /**
     * @ORM\Column(type="boolean")
     */
    private $active;

    /**
     * Subscriber constructor.
     */
    public function __construct()
    {
    }


    /**
     * Returns a list of possible periodicity values
     *
     * @return array
     */
    public static function getPeriodicityValues()
    {
        return array(
            'daily' => 'daily',
            'weekly' => 'weekly',
            'monthly' => 'monthly',
        );
    }
    /**
     * Returns a list of possible periodicity values as seconds
     *
     * @return array
     */
    public static function getPeriodicitySeconds()
    {
        return array(
            'daily' => 3600 * 24,
            'weekly' => 3600 * 24 * 7,
            'monthly' => 3600 * 24 * 30,
        );
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return Subscriber
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set categories
     *
     * @param string $categories
     * @return Subscriber
     */
    public function setCategories($categories)
    {
        $this->categories = $categories;

        return $this;
    }

    /**
     * Get categories
     *
     * @return string
     */
    public function getCategories()
    {
        return $this->categories;
    }

    /**
     * Set periodicity
     *
     * @param string $periodicity
     * @return Subscriber
     */
    public function setPeriodicity($periodicity)
    {
        $this->periodicity = $periodicity;

        return $this;
    }

    /**
     * Get periodicity
     *
     * @return string
     */
    public function getPeriodicity()
    {
        return $this->periodicity;
    }

    /**
     * Set lastSentMailAt
     *
     * @param \DateTime $lastSentMailAt
     * @return Subscriber
     */
    public function setLastSentMailAt($lastSentMailAt)
    {
        $this->lastSentMailAt = $lastSentMailAt;

        return $this;
    }

    /**
     * Get lastSentMailAt
     *
     * @return \DateTime
     */
    public function getLastSentMailAt()
    {
        return $this->lastSentMailAt;
    }

    /**
     * Set active
     *
     * @param boolean $active
     * @return Subscriber
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return boolean
     */
    public function active()
    {
        return $this->active;
    }

    /**
     * Get active
     *
     * @return boolean 
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Add categories
     *
     * @param \AppBundle\Entity\Category $categories
     * @return Subscriber
     */
    public function addCategory(\AppBundle\Entity\Category $categories)
    {
        $this->categories[] = $categories;

        return $this;
    }

    /**
     * Remove categories
     *
     * @param \AppBundle\Entity\Category $categories
     */
    public function removeCategory(\AppBundle\Entity\Category $categories)
    {
        $this->categories->removeElement($categories);
    }
}
