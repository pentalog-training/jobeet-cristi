<?php
namespace AppBundle\Tests\Repository;

use Doctrine\Bundle\DoctrineBundle\Command\CreateDatabaseDoctrineCommand;
use Doctrine\Bundle\DoctrineBundle\Command\DropDatabaseDoctrineCommand;
use Doctrine\Bundle\DoctrineBundle\Command\Proxy\CreateSchemaDoctrineCommand;
use Doctrine\Common\DataFixtures\Executor\ORMExecutor;
use Doctrine\Common\DataFixtures\Purger\ORMPurger;
use Symfony\Bridge\Doctrine\DataFixtures\ContainerAwareLoader;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Output\NullOutput;

class JobRepositoryTest extends WebTestCase
{
    private $em;
    private $application;

    public function setUp()
    {
        static::$kernel = static::createKernel();
        static::$kernel->boot();

        $this->application = new Application(static::$kernel);

        // drop the database
        $command = new DropDatabaseDoctrineCommand();
        $this->application->add($command);
        $input = new ArrayInput(array(
            'command' => 'doctrine:database:drop',
            '--force' => true,
        ));
        $command->run($input, new NullOutput());

        // we have to close the connection after the database se we don't get
        // "No database selected" error
        $connection = $this->application
            ->getKernel()
            ->getContainer()
            ->get('doctrine')
            ->getConnection();
        if ($connection->isConnected()) {
            $connection->close();
        }

        // create the database
        $command = new CreateDatabaseDoctrineCommand();
        $this->application->add($command);
        $input = new ArrayInput(array(
            'command' => 'doctrine:database:create',
        ));
        $command->run($input, new NullOutput());

        // create schema
        $command = new CreateSchemaDoctrineCommand();
        $this->application->add($command);
        $input = new ArrayInput(array(
            'command' => 'doctrine:schema:create'
        ));
        $command->run($input, new NullOutput());

        // get the Entity Manager
        $this->em = static::$kernel->getContainer()
            ->get('doctrine')
            ->getManager();

        // load fixtures
        $client = static::createClient();
        $loader = new ContainerAwareLoader($client->getContainer());
        $loader->loadFromDirectory(static::$kernel->locateResource('@AppBundle/DataFixtures/ORM'));
        $purger = new ORMPurger($this->em);
        $executor = new ORMExecutor($this->em, $purger);
        $executor->execute($loader->getFixtures());
    }

    public function testCountActiveJobs()
    {
        $query = $this->em->createQuery('SELECT c FROM AppBundle:category c');
        $categories = $query->getResult();

        foreach ($categories as $category) {
            $query = $this->em->createQuery('
                SELECT COUNT(j.id)
                FROM AppBundle:job j
                WHERE j.category = :category AND j.expiresAt > :date
            ');
            $query->setParameter('category', $category->getId());
            $query->setParameter('date', date('Y-m-d H:i:s', time()));
            $jobsDb = $query->getSingleScalarResult();

            $jobsRep = $this->em->getRepository('AppBundle:Job')->countActiveJobs($category->getId());
            // This test will verify if the value returned by the countActiveJobs() function coincides
            // with the number of active jobs for a given category from the database

            $this->assertEquals($jobsRep, $jobsDb);
        }
    }

    public function testGetActiveJobs()
    {
        $query = $this->em->createQuery('SELECT c FROM AppBundle:category c');
        $categories = $query->getResult();

        foreach ($categories as $category) {
            $query = $this->em->createQuery('
                SELECT COUNT(j.id)
                FROM AppBundle:job j
                WHERE j.expiresAt > :date AND j.category = :category
            ');
            $query->setParameter('date', date('Y-m-d H:i:s', time()));
            $query->setParameter('category', $category->getId());
            $jobsDb = $query->getSingleScalarResult();

            $jobsRep = $this->em->getRepository('AppBundle:Job')->getActiveJobs($category->getId(), null, null);
            // This test tells if the number of active jobs for a given category from the database is
            // the same as the value returned by the function
            $this->assertEquals($jobsDb, count($jobsRep));
        }
    }

    public function testGetActiveJob()
    {
        $query = $this->em->createQuery('SELECT j FROM AppBundle:Job j WHERE j.expiresAt > :date');
        $query->setParameter('date', date('Y-m-d H:i:s', time()));
        $query->setMaxResults(1);
        $jobDb = $query->getSingleResult();

        $jobRep = $this->em->getRepository('AppBundle:Job')->getActiveJob($jobDb->getId());

        // If the job is active, the getActiveJob() method should return a non-null value
        $this->assertNotNull($jobRep);

        $query = $this->em->createQuery('SELECT j FROM AppBundle:Job j WHERE j.expiresAt < :date');
        $query->setParameter('date', date('Y-m-d H:i:s', time()));
        $query->setMaxResults(1);
        $jobExpired = $query->getSingleResult();

        $jobRep = $this->em->getRepository('AppBundle:Job')->getActiveJob($jobExpired->getId());
        // If the job is expired, the getActiveJob() method should return a null value
        $this->assertNull($jobRep);
    }

    protected function tearDown()
    {
        parent::tearDown();
        $this->em->close();
    }
}