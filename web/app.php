<?php
use Symfony\Component\Debug\Debug;
use Symfony\Component\HttpFoundation\Request;

/**
 * @var Composer\Autoload\ClassLoader
 */
$loader = require __DIR__ . '/../app/autoload.php';

// Use APC for autoloading to improve performance.
// Change 'sf2' to a unique prefix in order to prevent cache key conflicts
// with other applications also using APC.

$environment = 'prod';
$debug = false;
if (getenv('APP_ENV') == 'dev') {
    Debug::enable();
    $environment = 'dev';
    $debug = true;
} else {
    include_once __DIR__ . '/../app/bootstrap.php.cache';
}

require_once __DIR__.'/../app/AppKernel.php';

$kernel = new AppKernel($environment, $debug);
$kernel->loadClassCache();
$request = Request::createFromGlobals();
$response = $kernel->handle($request);
$response->send();
$kernel->terminate($request, $response);
