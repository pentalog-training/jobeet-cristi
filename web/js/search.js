$(document).ready(function () {
    $('#search_keywords')
        .keyup(function (key) {
            if (this.value.length >= 3 || this.value == '') {
                $.get(
                    $(this).closest('form').attr('action'),
                    {query: this.value},
                    function (data) {
                        $('#jobs').html(data);
                    }
                );
            }
        });
});
