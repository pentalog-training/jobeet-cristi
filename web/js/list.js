$(document).ready(function () {
    var $holders = $('.category-job-list-holder');
    var categoryId;

    $holders.each(function () {
        var $holder = $(this);
        categoryId = $holder.data('category-id');

        $.ajax({
            url: "/get/jobs/" +  categoryId
        })
            .done(function(html) {
                $holder.html(html);
            })
            .fail(function () {
                $holder.html('<p>Error occured while retrieving jobs.</p>');
            });
    });
});